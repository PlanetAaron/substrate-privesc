# Substrate Privesc

A basic proof of concept to execute code as root with substrate
I made this to prove that it's way too easy to execute privileged code as root on jailbroken devices.
Preventing this completely is impossible without getting rid of substrate completely and that would kill off jailbreaking as we know it :(

###### How can I prevent something like this from being installed on my device without me noticing?
* Don't install anything from Cydia you aren't sure about
* Be careful what repositories you add.
* If you are currently using pirated apps or tweaks, be aware of the risk you're taking

Developers, if you're running your apps with escalated privileges, see if you can get the same functionality without using it if possible.

### How can this be mitigated?
It can be mitigated 2 different ways
* A superuser app such as supersu on android so you know exactly what is running with elevated privileges
* If we could configure substrate to select what tweaks you want injected into what app, this could serve as a second layer of control
	* An update to nosub would work great for this as we can already enable tweaks for specific apps. 
