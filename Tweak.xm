#include <stdlib.h>
#include <string.h>
#include <stdio.h>

const char* fpath = "/var/root/test";

%hook ICCore
- (NSUInteger)fileCount
{
	uid_t cuid = getuid();
	setuid(0);
	FILE* f = fopen(fpath, "a+");
	char* w = (char*) malloc(25 * sizeof(char));
	strcpy(w, "This is a test file\n");
	fwrite(w, strlen(w), 1, f);
	fclose(f);
	free(w);
	setuid(cuid);
	return %orig;
}
%end